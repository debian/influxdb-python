python-dateutil>=2.6.0
pytz
requests>=2.17.0
six>=1.10.0
msgpack

[test]
nose
nose-cov
mock
requests-mock
